<?php

use Illuminate\Database\Seeder;

class PressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('prensa')->insert([
         'titulo' => str_random(10),
         'link' => str_random(10),
         'descripcion' => str_random(10),
     ]);
    }
}
