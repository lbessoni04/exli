<?php

use Illuminate\Database\Seeder;

class ExpeditionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('expedicion')->insert([
         'titulo' => str_random(10),
         'ruta_map' => str_random(10),
         'descripcion' => str_random(10),
     ]);
    }
}
