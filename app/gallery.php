<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{
  protected $table = 'galeria';

  protected $fillable=[
  'titulo',
  'expedicion_id',
  'ruta_img',
  'descripcion',
  ];

  public function expedition()
  {
      return $this->belongsTo(Expedition::class);
  }
}
