<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sponsor;
use App\Http\Requests\StoreSponsorRequest;
use App\Http\Requests\UpdateSponsorRequest;
use Illuminate\Support\Facades\Input;

class SponsorsController extends Controller
{
  public function index()
  {
    $sponsors = Sponsor::all();
    return view('sponsors.index')->with(['sponsors' => $sponsors]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      $sponsor = new Sponsor;
      return view('sponsors.create')->with(['sponsor' => $sponsor]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreSponsorRequest $request)
  {
      $sponsor = new Sponsor;
      $sponsor->nombre = $request->get('nombre');
      $sponsor->url = $request->get('url');
      $sponsor->save();
      if (Input::hasFile('imagen')){
          $file = Input::file('imagen');
          $filename = $sponsor->id.'_logo'.'.jpg';
          Input::file('imagen')->move(public_path('images/sponsors'), $filename);
          $filename = "/images/sponsors/".$filename;
          $sponsor->ruta_img = $filename;
      }
      $sponsor->save();
      return redirect()->route('listSponsors');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Sponsor $sponsor)
  {
      return view('sponsors.edit')->with(['sponsor' => $sponsor]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateSponsorRequest $request, Sponsor $sponsor)
  {
      $sponsor->update($request->only(
        'nombre',
        'url'));
        if (Input::hasFile('imagen')){
            $file = Input::file('imagen');
            $filename = $sponsor->id.'_logo'.'.jpg';
            Input::file('imagen')->move(public_path('images/sponsors'), $filename);
            $filename = "/images/sponsors/".$filename;
            $sponsor->ruta_img = $filename;
        }
        $sponsor->save();

      return redirect()->route('listSponsors');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function delete(Sponsor $sponsor)
  {
      $sponsor->delete();
      return redirect()->route('listSponsors');
  }
}
