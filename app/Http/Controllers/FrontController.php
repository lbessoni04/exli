<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sponsor;
use DB;


class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function about(){
      return view('about');
    }

    public function welcome() {

        $sponsors = Sponsor::All();
        return view('welcome')->with(['sponsors' => $sponsors]);
}

  public function contact(){
    return view('contact');
  }

  public function faq(){
    return view('faqs');
  }

}
