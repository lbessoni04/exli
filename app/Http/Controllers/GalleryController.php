<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreGalleryRequest;
use App\Http\Requests\UpdateGalleryRequest;
use Illuminate\Support\Facades\Input;
use App\Gallery;
use App\Expedition;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expeditions = Expedition::All();
        return view('gallery.index')->with('expeditions', $expeditions);
    }

    public function showGallery(Expedition $expedition)
    {
        $id_expedition = $expedition->id;
        $gallery = Gallery::where('expedicion_id', $id_expedition)->orderBy('id', 'asc')->get();
        return view('gallery.show_gallery')->with(['gallery' => $gallery, 'expedition' => $expedition]);
    }

    public function showPhoto( Expedition $expedition, Gallery $photo)
    {
        return view('gallery.show_photo')->with(['photo' => $photo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Expedition $expedition)
    {
        $photo = new Gallery;
        return view('gallery.create')->with(['photo' => $photo, 'expedition' => $expedition]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGalleryRequest $request)
    {
        $photo = new Gallery;
        $photo->titulo = $request->get('titulo');
        $photo->expedicion_id = $request->get('expedicion_id');
        $photo->descripcion = $request->get('descripcion');
        $photo->save();
        if (Input::hasFile('imagen'))
        {
            $file = Input::file('imagen');
            $filename = $photo->id.'.jpg';
            $ruta = '/images/gallery/'.$photo->expedicion_id.'/';
            Input::file('imagen')->move(public_path($ruta), $filename);
            $photo->ruta_img = $ruta.$filename;
        }
        $photo->save();
        return redirect()->route('showPhoto', ['expedition' => $photo->expedicion_id, 'photo' => $photo->id]);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Expedition $expedition, Gallery $photo)
    {
      $expeditions = Expedition::All();
        return view('gallery.edit')->with(['photo' => $photo, 'expeditions' => $expeditions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGalleryRequest $request, Expedition $expedition, Gallery $photo)
    {
        $photo->update($request->only(
            'titulo',
            'expedicion_id',
            'descripcion'
          ));
          if (Input::hasFile('imagen'))
          {
              $file = Input::file('imagen');
              $filename = $photo->id.'.jpg';
              $ruta = '/images/gallery/'.$photo->expedicion_id.'/';
              Input::file('imagen')->move(public_path($ruta), $filename);
              $photo->ruta_img = $ruta.$filename;
              $photo->save();
          }
          return redirect()->route('showPhoto', ['expedition' => $photo->expedicion_id, 'photo' => $photo->id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Expedition $expedition, Gallery $photo)
    {
        $photo->delete();
        return redirect()->route('showGallery', ['expedition' => $expedition->id]);
    }
}
