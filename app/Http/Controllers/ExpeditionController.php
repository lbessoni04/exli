<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreExpeditionRequest;
use App\Http\Requests\UpdateExpeditionRequest;
use Illuminate\Support\Facades\Input;
use App\expedition;

class ExpeditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(expedition $expedition){
        $expeditions = Expedition::All();
        return view('expedition.index')->with(['expeditions' => $expeditions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $expedition = new Expedition;
        return view('expedition.create')->with(['expedition' => $expedition]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExpeditionRequest $request){
      $expedition = new Expedition;
      $expedition->titulo = $request->get('titulo');
      $expedition->ruta_map = $request->get('imagen');
      $expedition->descripcion = $request->get('descripcion');
      $expedition->save();
      //Logo de la expedicion
      if (Input::hasFile('imagen')){
          $file = Input::file('imagen');
          $filename = $expedition->id.'_logo'.'.jpg';
          Input::file('imagen')->move(public_path('images/expeditions'), $filename);
          $filename = "/images/expeditions/".$filename;
          $expedition->ruta_img = $filename;
      }
      //Imagen del mapa
      if (Input::hasFile('imagen1')){
          $file = Input::file('imagen1');
          $filename = $expedition->id.'_map'.'.jpg';
          Input::file('imagen1')->move(public_path('images/expeditions'), $filename);
          $filename = "/images/expeditions/".$filename;
          $expedition->ruta_map = $filename;
      }
      $expedition->save();
      return redirect()->route('showExpedition',['expedition' => $expedition->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Expedition $expedition){
      return view('expedition.show')->with(['expedition' => $expedition]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Expedition $expedition){
        return view('expedition.edit')->with(['expedition' => $expedition]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateExpeditionRequest $request, Expedition $expedition){
        $expedition->update($request->only(
          'titulo',
          'descripcion'));
          //Actualizar Logo
        if (Input::hasFile('imagen')){
          $file = Input::file('imagen');
          $filename = $expedition->id.'_logo'.'.jpg';
          Input::file('imagen')->move(public_path('images/expeditions'), $filename);
          $filename = "/images/expeditions/".$filename;
          $expedition->ruta_img = $filename;
          $expedition->save();
        }
        //Actualizar Mapa
        if (Input::hasFile('imagen1')){
          $file = Input::file('imagen1');
          $filename = $expedition->id.'_map'.'.jpg';
          Input::file('imagen1')->move(public_path('images/expeditions'), $filename);
          $filename = "/images/expeditions/".$filename;
          $expedition->ruta_map = $filename;
          $expedition->save();
        }

        return redirect()->route('showExpedition', ['expedition' => $expedition->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Expedition $expedition){
        $expedition->delete();
        return redirect()->route('listExpedition');
    }
}
