<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Press;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StorePressRequest;
use App\Http\Requests\UpdatePressRequest;

class PressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $press = Press::all();
      return view('press.index')->with(['press' => $press]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $news = new Press;
        return view('press.create')->with(['news' => $news]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePressRequest $request)
    {
        $news = new Press;
        $news->titulo = $request->get('titulo');
        $news->link = $request->get('link');
        $news->descripcion = $request->get('descripcion');
        $news->save();
        return redirect()->route('showPress', ['news' => $news->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Press $news)
    {
        return view('press.show')->with(['news' => $news]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Press $news)
    {
        return view('press.edit')->with(['news' => $news]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePressRequest $request, Press $news)
    {
        $news->update($request->only(
          'titulo',
          'link',
          'descripcion'));
        return redirect()->route('showPress', ['news' => $news->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Press $news)
    {
        $news->delete();
        return redirect()->route('listPress');
    }
}
