<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class expedition extends Model
{
  protected $table = 'expedicion';

  protected $fillable=[
  'titulo',
  'ruta_img',
  'ruta_map',
  'descripcion',
  ];
}
