<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class press extends Model
{
  protected $table = 'prensa';

  protected $fillable=[
    'titulo',
    'link',
    'descripcion'
  ];
}
