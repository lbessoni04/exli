@extends('layouts.app')

@section('content')
<div class="container">
  @if(Auth::check())
  <div class="row">
    <div class="col-sm-1">
      <a title="editar" href="{{ route('editPress', ['news' => $news->id] ) }}" class="btn btn-primary"><i class="far fa-edit" aria-hidden="true"></i></a>
    </div>
    <div class="col-sm-1">
      <a title="eliminar" class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#delete_register{{$news->id}}"><i class="fa fa-trash"></i></a>
    </div>

    <div id="delete_register{{$news->id}}" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">Eliminar Noticia</h4>
                </div>
                <div class="modal-body">
                    <h4>
                      &iquest;Seguro que desea eliminarla?
                    </h4>
                </div>
                <form action="{{route('deletePress', ['id' => $news->id])}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
  @endif
  <div class="row">
    <div class="col-xs-4 col-sm-4">
      <h1>{{$news->titulo}}</h1>
    </div>
  </div>

  <div class="container" style="height: 25px"></div>

  <div class="row">
    <div class="col-xs-4 col-sm-8">
      <p>{{$news->descripcion}}</p>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-4 col-sm-8">
      <h3>Fuente: <a href="{{$news->link}}">{{$news->link}}</a></h3>
    </div>
  </div>

  <div class="container" style="height: 50px"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="form-group pull-right">
        <a class="btn btn-primary" href="{{ route('listPress') }}">Volver</a>
      </div>
    </div>
  </div>

</div>
@endsection
