@extends('layouts.app')

@section('content')
<div class="container">
<div class="container" style="height: 25px"></div>
  <form action="{{route('storePress')}}" files="true" class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="x_title">
      <h2>Agregar Noticia</h2>
    </div>
    <div class="container" style="height: 25px"></div>
    <div class="x_content">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12"><h3>T&iacute;tulo<span class="required">*</span></label></h3>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" name="titulo" id="titulo" required="required" class="form-control col-md-7 col-xs-12" value="{{old('titulo')}}">
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12"><h3>URL de la noticia<span class="required">*</span></label></h3>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" name="link" id="link" required="required" class="form-control col-md-7 col-xs-12" value="{{old('link')}}">
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12"><h3>Descripci&oacute;n <span class="required">*</span></label></h3>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <textarea id="descripcion" class="form-control col-md-7 col-xs-12" name="descripcion" rows="8" cols="80">{{ old('descripcion') }}</textarea>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group pull-right">
                  <button type="submit" class="btn btn-success">Guardar</button>
                  <a href="{{ route('listPress') }}" class="btn btn-primary">Cancelar</a>
              </div>
          </div>
      </div>
    </div>
  </form>
</div>
@endsection
