@extends('layouts.app')

@section('content')
<div class="container">
  <div class="x_title">
    <div class="row">
      <h1 style="color: red">{{$photo->titulo}}</h1>
    </div>
    @if(Auth::check())
      <a title="editar" href="{{ route('editPhoto', ['photo' => $photo->id, 'expedition' => $photo->expedicion_id] ) }}" class="btn btn-primary"><i class="far fa-edit" aria-hidden="true"></i></a>
      <a title="eliminar" class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#delete_register{{$photo->id}}"><i class="fa fa-trash"></i></a>

      <div id="delete_register{{$photo->id}}" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel2">Eliminar Foto</h4>
                  </div>
                  <div class="modal-body">
                      <h4>
                        &iquest;Seguro que desea eliminarla?
                      </h4>
                  </div>
                  <form action="{{route('deletePhoto', ['id' => $photo->id, 'expedition' => $photo->expedicion_id])}}" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-danger">Eliminar</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
    @endif
  </div>
  <div class="row">

        <div class="col-xs-4 col-sm-4">
          <div class="photo-container"><img src="{{$photo->ruta_img}}"></a></div>
        </div>

  </div>

  <div class="container" style="height: 50px"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="form-group pull-right">
        <a class="btn btn-primary" href="{{ route('showGallery', ['expedition' => $photo->expedicion_id]) }}">Volver</a>
      </div>
    </div>
  </div>
</div>
@endsection
