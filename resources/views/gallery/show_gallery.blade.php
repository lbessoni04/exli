@extends('layouts.app')

@section('content')
<div class="container">
  <div class="x_title">
    <div class="row">
      <h1 style="color: red">Galeria de {{$expedition->titulo}}</h1>
    </div>
    @if(Auth::check())
    <div class=pull-right> <a href="{{ route('createPhoto', ['expedition' => $expedition->id]) }}" class="btn btn-success">Nueva Foto</a></div>
    @endif
  </div>
  <div class="row">
    @foreach($gallery as $photo)
        <div class="col-xs-4 col-sm-4">
          <a href="{{route('showPhoto', ['expedition' => $photo->expedicion_id, 'photo' => $photo->id])}}"><div class="container-img"><img src="{{$photo->ruta_img}}"></div></a>
        </div>
    @endforeach
  </div>
</div>
@endsection
