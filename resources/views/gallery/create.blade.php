@extends('layouts.app')

@section('content')
<div class="container">
<div class="container" style="height: 25px"></div>
  <form action="{{route('storePhoto', ['expedition' => $expedition])}}" files="true" class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="x_title">
      <h2>Agregar Foto</h2>
    </div>
    <div class="container" style="height: 25px"></div>
    <div class="x_content">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12"><h3>T&iacute;tulo<span class="required">*</span></label></h3>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" name="titulo" id="titulo" required="required" class="form-control col-md-7 col-xs-12" value="{{old('titulo')}}">
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="expedicion_id" id="expedicion_id" required="required" class="form-control col-md-7 col-xs-12" value="{{$expedition->id}}">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12"><h3>Expedici&oacute;n</label></h3>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" disabled name="expedicion" id="expedicion" required="required" class="form-control col-md-7 col-xs-12" value="{{$expedition->titulo}}">
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 profile_left">
          <div class="form-group">
            <div class="profile_img">
                <!-- end of image cropping -->
                <label for="logo"><h3>Imagen</h3></label>
                <div class="container-img"><img id="cambiar_imagen" src="/./images/unknown.jpg" class="imagen" /> </div>
                <input required="required" class="btn " id="imagen" name="imagen" type="file" onchange="readURL(this);">
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12"><h3>Descripci&oacute;n <span class="required">*</span></label></h3>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <textarea id="descripcion" class="form-control col-md-7 col-xs-12" name="descripcion" rows="8" cols="80">{{ old('descripcion') }}</textarea>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group pull-right">
                  <button type="submit" class="btn btn-success">Guardar</button>
                  <a href="{{ route('showGallery', ['expedition' => $expedition]) }}" class="btn btn-primary">Cancelar</a>
              </div>
          </div>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
        function readURL(input) {
          console.log(input.id)
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#cambiar_imagen').attr('src', e.target.result);

                }
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection
