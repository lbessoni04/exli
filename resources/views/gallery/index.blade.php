@extends('layouts.app')

@section('content')
<div class="container">
  <div class="x_title">
    <div class="row">
      <h1 style="color: red">Galer&iacute;a</h1>
    </div>
  </div>
  <div class="row">
    @foreach($expeditions as $expedition)

      <div class="container" style="height: 100px"></div>
        <div class="col-xs-4 col-sm-4">
          <div class="container-img"><img src="{{$expedition->ruta_img}}"></div>
        </div>
        <div class="row">
          <div class="col-xs-4 col-sm-4">
            <a class="nav-link3" href="{{route('showGallery', ['expedition' => $expedition->id])}}"><h2> {{$expedition->titulo}} </h2> </a>
          </div>
        </div>
    @endforeach
  </div>
</div>
@endsection
