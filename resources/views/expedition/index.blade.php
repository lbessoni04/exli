@extends('layouts.app')

@section('content')
<div class="container">
  <div class="x_title">
    <div class="row">
      <h1 style="color: red">Expediciones</h1>
    </div>
    @if(Auth::check())
    <div class=pull-right> <a href="{{ route('createExpedition') }}" class="btn btn-success">Nueva Expedici&oacute;n</a></div>
    @endif
  </div>
  <div class="row">
    @foreach($expeditions as $expedition)
    <div class="container" style="height: 50px"></div>
      <div class="col-xs-4 col-sm-4">
        <div class="container-img"><img src="{{$expedition->ruta_img}}"></div>
      </div>
      <div class="col-xs-4 col-sm-8">
        <a class="nav-link3" href="{{route('showExpedition', ['expedition' => $expedition->id])}}"><h2> {{$expedition->titulo}} </h2> </a>

        @if(Auth::check())
          <a title="editar" href="{{ route('editExpedition', ['expedition' => $expedition->id] ) }}" class="btn btn-primary"><i class="far fa-edit" aria-hidden="true"></i></a>
          <a title="eliminar" class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#delete_register{{$expedition->id}}"><i class="fa fa-trash"></i></a>
        @endif
      </div>

      <div id="delete_register{{$expedition->id}}" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel2">Eliminar Expedici&oacute;n {{$expedition->id}}</h4>
                  </div>
                  <div class="modal-body">
                      <h4>
                        &iquest;Seguro que desea eliminarla?
                      </h4>
                  </div>
                  <form action="{{route('deleteExpedition', ['id' => $expedition->id])}}" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-danger">Eliminar</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
    @endforeach
  </div>
</div>
@endsection
