@extends('layouts.app')

@section('content')
<div class="container">
  <div class="x_title">
    <div class="row">
      <h1 style="color: red">Sponsors</h1>
    </div>
    @if(Auth::check())
    <div class=pull-right> <a href="{{ route('createSponsor') }}" class="btn btn-success">Nuevo Sponsor</a></div>
    @endif
  </div>
  <div class="container" style="height: 25px"></div>
  <div class="row">
    @foreach($sponsors as $sponsor)
    @if(Auth::check())
      <div class="col-xs-2 col-sm-2">
        <a title="editar" href="{{ route('editSponsor', ['sponsor' => $sponsor->id] ) }}" class="btn btn-primary"><i class="far fa-edit" aria-hidden="true"></i></a>
        <a title="eliminar" class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#delete_register{{$sponsor->id}}"><i class="fa fa-trash"></i></a>
      </div>
      <div id="delete_register{{$sponsor->id}}" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel2">Eliminar Sponsor</h4>
                  </div>
                  <div class="modal-body">
                      <h4>
                        &iquest;Seguro que desea eliminarlo?
                      </h4>
                  </div>
                  <form action="{{route('deleteSponsor', ['id' => $sponsor->id])}}" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-danger">Eliminar</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
    @endif



        <div class="col-xs-4 col-sm-4" style="margin-top:25px">
          <a href="{{$sponsor->url}}"><div class="container-sponsor"><img src="{{$sponsor->ruta_img}}"></div></a>
        </div>

    @endforeach
  </div>
</div>
@endsection
