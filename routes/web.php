<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Authentication Routes...
$this->get('diogenes/login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('diogenes/login', 'Auth\LoginController@login');
$this->post('diogenes/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('diogenes/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('diogenes/register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('diogenes/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
 $this->post('diogenes/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('diogenes/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('diogenes/password/reset', 'Auth\ResetPasswordController@reset');

//Rutas de página
Route::get('/', 'FrontController@welcome')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'FrontController@about')->name('frontAbout');
Route::get('/contact', 'FrontController@contact')->name('frontContact');
Route::get('/FAQ', 'FrontController@faq')->name('frontFAQ');

//Rutas Expediciones
Route::get('/expeditions','ExpeditionController@index')->name('listExpedition');
Route::post('/expeditions/{expedition}/edit','ExpeditionController@update')->name('updateExpedition');
Route::get('/expeditions/{expedition}/edit','ExpeditionController@edit')->name('editExpedition');
Route::get('/expeditions/create','ExpeditionController@create')->name('createExpedition');
Route::post('/expeditions', 'ExpeditionController@store')->name('storeExpedition');
Route::get('/expeditions/{expedition}', 'ExpeditionController@show')->name('showExpedition');
Route::delete('/expeditions/{expedition}', 'ExpeditionController@delete')->name('deleteExpedition');

//Rutas Prensa
Route::get('/press','PressController@index')->name('listPress');
Route::post('/press/{news}/edit','PressController@update')->name('updatePress');
Route::get('/press/{news}/edit','PressController@edit')->name('editPress');
Route::get('/press/create','PressController@create')->name('createPress');
Route::post('/press', 'PressController@store')->name('storePress');
Route::get('/press/{news}', 'PressController@show')->name('showPress');
Route::delete('/press/{news}', 'PressController@delete')->name('deletePress');

//Rutas Galeria
Route::get('/gallery','GalleryController@index')->name('listGallery');
Route::post('/gallery/{expedition}/{photo}/edit','GalleryController@update')->name('updatePhoto');
Route::get('/gallery/{expedition}/{photo}/edit','GalleryController@edit')->name('editPhoto');
Route::get('/gallery/{expedition}/create','GalleryController@create')->name('createPhoto');
Route::post('/gallery/{expedition}', 'GalleryController@store')->name('storePhoto');
Route::get('/gallery/{expedition}', 'GalleryController@showGallery')->name('showGallery');
Route::get('/gallery/{expedition}/{photo}', 'GalleryController@showPhoto')->name('showPhoto');
Route::delete('/galley/{expedition}/{photo}', 'GalleryController@delete')->name('deletePhoto');

//Rutas Sponsors
Route::get('/sponsors', 'SponsorsController@index')->name('listSponsors');
Route::post('/sponsors/{sponsor}/edit','SponsorsController@update')->name('updateSponsor');
Route::get('/sponsors/{sponsor}/edit','SponsorsController@edit')->name('editSponsor');
Route::get('/sponsors/create','SponsorsController@create')->name('createSponsor');
Route::post('/sponsors', 'SponsorsController@store')->name('storeSponsor');
Route::delete('/sponsor/{sponsor}', 'SponsorsController@delete')->name('deleteSponsor');
